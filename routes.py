from flask import Flask, render_template, request, flash
from flask_mail import Mail, Message
from form import ContactForm

app = Flask(__name__)
app.config.from_pyfile('config.cfg')

 # /
@app.route('/')
def home():
	return render_template('home.html')

# /about
@app.route('/about')
def about():
	return render_template('about.html')

# /contact
@app.route('/contact', methods=['GET', 'POST'])
def contact():
	form = ContactForm()

	if request.method == 'POST':
		if form.validate() == False:
			flash('Fields Required!')
			return render_template('contact.html', form=form)
		else:
			return '<h1> New email from {}.'.format(form.name.data)

	elif request.method == 'GET':
		return render_template('contact.html', form=form)

if __name__ == '__main__':
	app.run(debug=True)
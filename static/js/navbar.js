var openBtn = document.querySelector('.mobile-btn');
var menu = document.querySelector('.nav-menu');
var closeBtn = document.querySelector('.close');
var overlay = document.querySelector('.overlay');

openBtn.addEventListener('click', function() {
	menu.className += ' open';
	overlay.className += ' open';
});

closeBtn.addEventListener('click', function () {
	menu.className = 'nav-menu';
	overlay.className = 'overlay';
});

window.addEventListener('click', function(e) {
	if(e.target === overlay) {
		menu.className = 'nav-menu';
		overlay.className = 'overlay';
	}
})